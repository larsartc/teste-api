<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedicoEspecialidade extends Model
{
    protected $table = 'medicos_especialidades';
    protected $fillable = ['medico_id', 'especialidade_id'];
    public $timestamps = false;
}