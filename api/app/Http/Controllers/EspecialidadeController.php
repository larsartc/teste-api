<?php

namespace App\Http\Controllers;

use App\Especialidade;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EspecialidadeController extends Controller
{
    public function get(Request $request)
    {
        return response()->json(Especialidade::all());
    }
}