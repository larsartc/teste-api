<?php

namespace App\Http\Controllers;

use App\Medico;
use App\MedicoEspecialidade;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MedicoController extends Controller
{
    public function get()
    {
        return response()->json(Medico::with('especialidades')->get());
    }

    public function getById($id) 
    {
        return response()->json(Medico::with('especialidades')->find($id));
    }

    public function save(Request $request)
    {
        $medico = [
            'nome' => $request->nome,
            'crm' => $request->crm,
            'telefone' => $request->telefone,
        ];
        
        return response()->json(Medico::updateOrCreate(['id' => $request->id], $medico));
    }

    public function remove($id)
    {
        MedicoEspecialidade::where('medico_id', $id)->delete();
        Medico::where('id', $id)->delete();
    }
}