<?php

namespace App\Http\Controllers;

use App\MedicoEspecialidade;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MedicoEspecialidadeController extends Controller
{
    public function save(Request $request)
    {
        $medicoEspecialidade = [
            'medico_id' => $request->medico_id,
            'especialidade_id' => $request->especialidade_id
        ];
        
        return response()->json(MedicoEspecialidade::updateOrCreate(['medico_id' => $request->medico_id, 'especialidade_id' => $request->especialidade_id], $medicoEspecialidade));
    }

    public function remove($id)
    {
        MedicoEspecialidade::where('id', $id)->delete();
    }
}