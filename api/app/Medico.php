<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medico extends Model
{
    protected $table = 'medicos';
    protected $fillable = ['nome', 'crm', 'telefone'];
    public $timestamps = false;

    public function especialidades()
    {
        return $this->hasMany('App\MedicoEspecialidade');
    }
}