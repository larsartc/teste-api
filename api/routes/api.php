<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'medicos'], function() {
    Route::get('/', 'MedicoController@get');
    Route::get('/{id}', 'MedicoController@getById');
    Route::post('/', 'MedicoController@save');
    Route::delete('/{id}', 'MedicoController@remove');

    Route::post('/especialidade', 'MedicoEspecialidadeController@save');
    Route::delete('/especialidade/{id}', 'MedicoEspecialidadeController@remove');
});

Route::group(['prefix' => 'especialidades'], function() {
    Route::get('/', 'EspecialidadeController@get');
});