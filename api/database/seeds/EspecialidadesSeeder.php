<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EspecialidadesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('especialidades')->insert(['nome' => 'ALERGOLOGIA']);
        DB::table('especialidades')->insert(['nome' => 'ANGIOLOGIA']);
        DB::table('especialidades')->insert(['nome' => 'BUCO MAXILO']);
        DB::table('especialidades')->insert(['nome' => 'CARDIOLOGIA CLÍNICA']);
        DB::table('especialidades')->insert(['nome' => 'CARDIOLOGIA INFANTIL']);
        DB::table('especialidades')->insert(['nome' => 'CIRURGIA CABEÇA E PESCOÇO']);
        DB::table('especialidades')->insert(['nome' => 'CIRURGIA CARDÍACA']);
        DB::table('especialidades')->insert(['nome' => 'CIRURGIA DE CABEÇA/PESCOÇO']);
        DB::table('especialidades')->insert(['nome' => 'CIRURGIA DE TÓRAX']);
        DB::table('especialidades')->insert(['nome' => 'CIRURGIA GERAL']);
        DB::table('especialidades')->insert(['nome' => 'CIRURGIA PEDIÁTRICA']);
        DB::table('especialidades')->insert(['nome' => 'CIRURGIA PLÁSTICA']);
        DB::table('especialidades')->insert(['nome' => 'CIRURGIA TORÁCICA']);
        DB::table('especialidades')->insert(['nome' => 'CIRURGIA VASCULAR']);
        DB::table('especialidades')->insert(['nome' => 'CLÍNICA MÉDICA']);
    }
}
