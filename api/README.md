# Projeto

Medical Api

## Instalação

Será necessário possuir o composer instalado em sua máquina. Caso não tenha, acesse https://getcomposer.org/

Use o composer para instalar as dependencias

```bash
composer install
```

Utilize o arquivo .env.example para configurar a base de dados em seu .env local

Execute os comandos abaixo para criar a base de dados e preencher as informações iniciais.

``` php
php artisan migrate
php artisan db:seed
```