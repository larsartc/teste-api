import axios from 'axios';
import Environment from '../config/Environment';
import Medic from '../models/Medic';

class MedicService {
    get(): Promise<Medic[]> {
        return axios.get(`${Environment.api}medicos`);
    }
    getById(id: number): Promise<Medic> {
        return axios.get(`${Environment.api}medicos/${id}`);
    }
    save(medic: Medic): Promise<Medic> {
        return axios.post(`${Environment.api}medicos`, medic);
    }
    remove(id: number): Promise<void> {
        return axios.delete(`${Environment.api}medicos/${id}`);
    }
}

export default new MedicService();