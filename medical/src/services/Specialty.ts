import axios from 'axios';
import Especialty from 'models/Especialty';
import Environment from 'config/Environment';

class Specialty {

    get(): Promise<Especialty> {
        return axios.get(`${Environment.api}especialidades`);
    }

}

export default new Specialty();