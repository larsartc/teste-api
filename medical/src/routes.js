import Dashboard from "views/Dashboard.jsx";
import Medicos from "views/Medicos.jsx";

const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "pe-7s-graph",
    component: Dashboard,
    layout: "/admin"
  },
  {
    path: "/medics",
    name: "Medicos",
    icon: "pe-7s-user",
    component: Medicos,
    layout: "/admin"
  }
];

export default dashboardRoutes;
