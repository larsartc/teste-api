export default class Medic {
    id: number;
    nome: string;
    crm: string;
    telefone: string;

    constructor() {
        this.id = 0;
        this.nome = '';
        this.crm = '';
        this.telefone = '';
    }
}