const Environment = {
    prod: false,
    api: 'http://localhost:8000/api/'
};

export default Environment;