import React, { Component } from "react";
import { Button, Modal, Grid, Row, Col } from "react-bootstrap";
import { MDBDataTable, MDBBtn } from 'mdbreact';
import MultiSelect from "@kenshooui/react-multi-select";

import CustomButton from "components/CustomButton/CustomButton";
import Medic from '../models/Medic';

import medicService from '../services/Medic';
import specialtyService from '../services/Specialty';

class Medicos extends Component {
    constructor() {
        super()
        this.state = {
            medics: {},
            specialties: {},
            id: 0,
            nome: '',
            crm: '',
            telefone: '',
            selectedSpecialties: [],
            showNewModal: false,
            showEditModal: false
        };
    }

    handleEdit = async (id) => {
        const response = await medicService.getById(id);

        const medic = response.data;

        this.setState({
            id: medic.id,
            nome: medic.nome,
            crm: medic.crm,
            telefone: medic.telefone,
            showEditModal: true
        });
    }

    handleRemove = async (id) => {
        await medicService.remove(id);
        this.loadData();
    }

    loadSpecialties = async () => {
        const response = await specialtyService.get();

        const specialties = response.data.map(specialty => {
            return { id: specialty.id, label: specialty.nome }
        })

        this.setState({
            specialties: specialties
        })
    }

    loadData = async () => {
        let medics = await medicService.get();
        medics = medics.data.map(medic => {
            return {
                ...medic,
                edit: <MDBBtn color="purple" onClick={() => { this.handleEdit(medic.id) }} size="sm">Editar</MDBBtn>,
                remove: <MDBBtn color="red" onClick={() => { this.handleRemove(medic.id) }} size="sm">Remove</MDBBtn>
            }
        })
        this.setState({
            medics: {
                columns: [
                    {
                        label: 'Nome',
                        field: 'nome',
                        sort: 'asc',
                        width: 150
                    },
                    {
                        label: 'CRM',
                        field: 'crm',
                        sort: 'asc',
                        width: 270
                    },
                    {
                        label: 'Telefone',
                        field: 'telefone',
                        sort: 'asc',
                        width: 200
                    },
                    {
                        label: 'Editar',
                        field: 'edit',
                        sort: '',
                        width: 200
                    },
                    {
                        label: 'Deletar',
                        field: 'remove',
                        sort: '',
                        width: 200
                    }
                ],
                rows: medics
            }
        });
    }

    async componentDidMount() {
        this.loadSpecialties();
        this.loadData();
    }

    render() {

        const handleNewCloseModal = () => this.setState({ showNewModal: false });
        const handleNewShowModal = () => this.setState({ showNewModal: true });
        const handleEditCloseModal = () => this.setState({ showEditModal: false });

        const handleNewModalSubmit = async () => {

            console.log(this.state.selectedSpecialties);

            const medico = new Medic();

            medico.nome = this.state.nome;
            medico.crm = this.state.crm;
            medico.telefone = this.state.telefone;

            await medicService.save(medico);

            this.loadData();
            handleNewCloseModal();
        }

        const handleEditModalSubmit = async () => {
            const medico = new Medic();

            medico.id = this.state.id;
            medico.nome = this.state.nome;
            medico.crm = this.state.crm;
            medico.telefone = this.state.telefone;

            await medicService.save(medico);

            this.loadData();
            handleEditCloseModal();
        }

        const handleChangeName = (event) => {
            this.setState({
                nome: event.target.value
            })
        };

        const handleChangeCrm = (event) => {
            this.setState({
                crm: event.target.value
            })
        };

        const handleChangeTelefone = (event) => {
            this.setState({
                telefone: event.target.value
            })
        };

        const handleChangeEspecialidades = () => {
        }

        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <CustomButton onClick={handleNewShowModal} pullRight>Novo Médico</CustomButton>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={12}>
                            <MDBDataTable
                                striped
                                bordered
                                hover
                                data={this.state.medics}
                            />
                        </Col>
                    </Row>
                </Grid>


                <Modal show={this.state.showNewModal} onHide={handleNewCloseModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>Novo Médico</Modal.Title>
                    </Modal.Header>
                    <Modal.Body><form>
                        <div className="form-group">
                            <label>Nome</label>
                            <input name="nome" className="form-control" onChange={handleChangeName} />
                        </div>
                        <div className="form-group">
                            <label>CRM</label>
                            <input name="crm" className="form-control" onChange={handleChangeCrm} />
                        </div>
                        <div className="form-group">
                            <label>Telefone</label>
                            <input name="telefone" className="form-control" onChange={handleChangeTelefone} />
                        </div>
                        <hr></hr>
                        <div className="form-group">
                            <label>Especialidades</label>
                            <div>
                                <MultiSelect
                                    items={this.state.specialties}
                                    selectedItems={this.state.selectedSpecialties}
                                    onChange={this.handleChangeEspecialidades}
                                />
                            </div>
                        </div>
                        <div className="clearfix" />
                    </form>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleNewCloseModal}>Fechar</Button>
                        <Button variant="primary" onClick={handleNewModalSubmit}>Salvar</Button>
                    </Modal.Footer>
                </Modal>

                <Modal show={this.state.showEditModal} onHide={handleEditCloseModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>Editar Médico</Modal.Title>
                    </Modal.Header>
                    <Modal.Body><form>
                        <div className="form-group">
                            <label>Nome</label>
                            <input name="nome" className="form-control" onChange={handleChangeName} value={this.state.nome} />
                        </div>
                        <div className="form-group">
                            <label>CRM</label>
                            <input name="crm" className="form-control" onChange={handleChangeCrm} value={this.state.crm} />
                        </div>
                        <div className="form-group">
                            <label>Telefone</label>
                            <input name="telefone" className="form-control" onChange={handleChangeTelefone} value={this.state.telefone} />
                        </div>
                        <div className="clearfix" />
                    </form>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleEditCloseModal}>Fechar</Button>
                        <Button variant="primary" onClick={handleEditModalSubmit}>Salvar</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default Medicos;
